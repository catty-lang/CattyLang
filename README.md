

# CattyLang

[![Discord](https://img.shields.io/discord/809302664914796546?label=discord&logo=discord)](https://discord.gg/ypcMSagbtv) ![GitHub release (latest by date)](https://img.shields.io/github/v/release/CattyLang/CattyLang) ![GitHub issues](https://img.shields.io/github/issues/CattyLang/CattyLang)

The programming language of cats.


Catty is a localisable, cross-platform, compiled programming language designed by [Yiğit Cemal Öztürk](https://github.com/CadmiumC4). It is still being developed and cannot be used for now.

## Useful Repositories

- [**Compiler repository**](https://github.com/CattyLang/cattylang-compiler)
- [**Repository of Atatürk Runtime**](https://github.com/CattyLang/ataturk-rt)
- [**Repository of Atatürk Runtime for Mobile Devices**](https://github.com/CattyLang/ataturk-rt-mobile)
- [**Repository of WebMeow, a web runtime for Catty**](https://github.com/CattyLang/webmeow)

###### Footnote:
The rest of this repository is going to be written in Turkish, because I cannot betray my home country and my main language. However, I will create an English section for the notes.
